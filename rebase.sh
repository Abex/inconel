#!/bin/bash
set -e -x
pushd chromium/src
git fetch
COMMIT=$(python ../../find-tag.py "$1")
ROOT=$(git rev-parse ":/Disable extension store check")
git rebase --onto "$COMMIT" "$ROOT^" abex-fixes
echo "Run"
echo "gclient sync"
popd