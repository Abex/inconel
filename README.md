Inconel
========

Inconel is a alloy of Chromium and Nickel. Also it is the name of my Chromium fork. It is here as a set of scripts and
patches, because the Chromium repository is so large and fast-moving. This allows me to apply the patch to many versions,
without having to constant upload gigabytes of patches to my vcs.

Building
---------

Start with the [Chromium build Guide](https://www.chromium.org/developers/how-tos/get-the-code), then run
`apply-patch.sh` and rebuild.
