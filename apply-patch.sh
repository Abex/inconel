#!/bin/bash
set -e -x
pushd chromium/src
if ! git diff-index --quiet HEAD --; then
	echo "There are uncommited changes, please commit or stash them"
	exit 1
fi
COMMIT=$(python ../../find-tag.py "$1")

echo "Using commit $COMMIT"
BRANCHNAME="abex-fixes-build/$COMMIT"
echo "git branch -D $BRANCHNAME"
git branch -d "$BRANCHNAME" || true
git checkout -b "$BRANCHNAME" $COMMIT
if ! git apply ../../inconel.patch; then
	echo "Applying 3 way patch. Please resolve the conflicts"
	git apply -3 ../../inconel.patch
else
	git commit -a -m "Apply inconel.patch"
fi
set +x
echo "Run:"
echo "gclient sync"
echo "ninja -C out/Release mini-installer"