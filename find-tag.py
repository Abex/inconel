from __future__ import print_function
import subprocess
import sys

def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)

arg=sys.argv[1] if len(sys.argv)>=2 else ""
if arg!="":
	try:
		commit = subprocess.check_output("git","rev-parse",sys.argv[1])
		print(commit)
	except:
		pass

gittag = subprocess.Popen(["git","tag"], stdout=subprocess.PIPE)
vers={}
hiver=0
for tag in gittag.stdout:
	vp=tag.split(".")
	if len(vp)>2:
		vp=map(int,vp)
		if hiver<vp[0]:
			hiver=vp[0]
		if not vp[0] in vers:
			vers[vp[0]]=vp
		else:
			for idx,_ in enumerate(vp):
				if vp[idx]>vers[vp[0]][idx]:
					vers[vp[0]]=vp
					break
				if vp[idx]<vers[vp[0]][idx]:
					break
if len(vers)<2:
	eprint("git tag may have failed.")
	sys.exit(1)
targetVer = hiver-1 if arg=="" else int(arg)
if not targetVer in vers:
	eprint("cannot find {} in versions".format(targetVer))
	sys.exit(1)
print((".".join(map(str,vers[targetVer])))+"^")
