#!/bin/bash
set -e -x
pushd chromium/src
ROOT=$(git rev-parse ":/Disable extension store check")
git diff "$ROOT"^ abex-fixes > ../../inconel.patch
popd